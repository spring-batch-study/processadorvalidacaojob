package com.springbatch.processadorvalidacao.processor;

import com.springbatch.processadorvalidacao.dominio.Cliente;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.support.builder.CompositeItemProcessorBuilder;
import org.springframework.batch.item.validator.BeanValidatingItemProcessor;
import org.springframework.batch.item.validator.ValidatingItemProcessor;
import org.springframework.batch.item.validator.ValidationException;
import org.springframework.batch.item.validator.Validator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashSet;
import java.util.Set;

@Configuration
public class ProcessadorValidacaoProcessorConfig {

    private final Set<String> emails = new HashSet<>();

	// Para validações simples usando Bean Validation
	@Bean
	public ItemProcessor<Cliente, Cliente> procesadorValidacaoProcessor() throws Exception {
        return new CompositeItemProcessorBuilder<Cliente, Cliente>()
                .delegates(beanValidatingProcessor(), emailValidatingProcessor())
                .build();
	}

	private BeanValidatingItemProcessor<Cliente> beanValidatingProcessor() throws Exception {
        final BeanValidatingItemProcessor<Cliente> processor = new BeanValidatingItemProcessor<>();

        processor.afterPropertiesSet(); // necessário quando se usa validadores compostos
        // OPCIONAL: filtra o item caso esteja inválido e não vai impedir a execução do batch
        processor.setFilter(true);
        return processor;
    }

    private ValidatingItemProcessor<Cliente> emailValidatingProcessor() throws Exception {
        final ValidatingItemProcessor<Cliente> processor = new ValidatingItemProcessor<>();
        processor.setValidator(validator());
        processor.afterPropertiesSet(); // necessário quando se usa validadores compostos
        // OPCIONAL: filtra o item caso esteja inválido e não vai impedir a execução do batch
        processor.setFilter(true);
        return processor;
    }

    private Validator<Cliente> validator() {
        return new Validator<Cliente>() {

            @Override
            public void validate(final Cliente cliente) throws ValidationException {
                // verifica se há e-mail duplicado na execução
                if (emails.contains(cliente.getEmail())) {
                    throw new ValidationException(String.format("O cliente %s já foi processado!", cliente.getEmail()));
                }

            }

        };

    }

}
